# FPO UTILITY LIBRARY (utils)
#### Developer: [Phuong Duong](mailto:phuongduong@fpo.vn) from [FPO Co.,Ltd](http://www.fpo.vn)
---
# Welcome! 2 Things You Ought To Know First:
**1. What is FPO UTILITY LIBRARY ? -** This is a collection of some library that help Node.JS developer easy to develop their application and don't care about a little thing :)

**2. How to use ? -** Install via npm with command `npm install fpo.utils --save`
<br/><br/>

## Table of Contents
1. [DateTime library](#1-datetime-library)
1. [String library](#2-string-library)


<br/><br/>
# `1. DateTime library`
- toLocalDateTime
- dateToString
- dateTimeToString
- getISODate
- getTimeElapsed
- getRemainDay

<br/><br/>

# `2. String library`
- getFileExtension
- getFilename

<br/><br/>
