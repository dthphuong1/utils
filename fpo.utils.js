//#region============DateTime functions============

// Convert datetime to Local datetime with weekday, dd, mm, yyyy, hrs, min, sec, ii
function toLocateDateTime(dateTime) {
    var enWeekDay = [1, 2, 3, 4, 5, 6, 0];
    var viWeekDay = ['Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy', 'Chủ nhật']

    // request a weekday along with a long date
    var options = { weekday: 'short', year: 'numeric', month: '2-digit', day: '2-digit',
                    hour: '2-digit', minute: '2-digit', second: '2-digit',
                    timeZone: 'UTC', timeZoneName: 'short' };

    var localDate = (new Date(dateTime)).toLocaleDateString('vi-VN');
    var localTime = (new Date(dateTime)).toLocaleTimeString('vi-VN');
    
    var weekday = viWeekDay[enWeekDay.indexOf((new Date(dateTime)).getDay())];

    var _date = localDate.split('/');
    var dd =_date[1].length < 2 ? '0' + _date[1] : _date[1];
    var mm = _date[0].length < 2 ? '0' + _date[0] : _date[0];
    var yyyy = _date[2];

    var _time = localTime;
    var ii = (_time.indexOf('AM') != -1 ? 'AM' : 'PM');
    _time = _time.replace(' ' + ii, '');
    var hrs = _time.split(':')[0];
    var min = _time.split(':')[1];
    var sec = _time.split(':')[2];

    return {
        'weekDay': weekday,
        'dd': dd, 'mm': mm, 'yyyy': yyyy,
        'hrs': hrs, 'min': min, 'sec': sec,
        'ii': ii
    }
}

// Convert Date to String with format string
function dateToString(date, format) {
    date = toLocateDateTime(date);
    var day = date.dd,
        month = date.mm,
        year = date.yyyy;

    if (format.indexOf('dd') >= 0 && format.indexOf('mm') >= 0 && format.indexOf('yyyy') >= 0) {
        format = format.replace('dd', date.dd);
        format = format.replace('mm', date.mm);
        format = format.replace('yyyy', date.yyyy);
        return format;
    } else {
        return 'Wrong format. Format must be included `dd`, `mm`, `yyyy`';
    }
}


// Convert Date/Time to String (dd/mm/yyyy vào lúc hh:min ii)
function dateTimeToString(date) {
    date = toLocateDateTime(date);
    return date.dd + '/' + date.mm + '/' + date.yyyy + " vào lúc " + date.hrs + ":" + date.min + ' ' + date.ii;
}

// Get ISODate of start day and end day
function getISODate(date) {
    date = toLocateDateTime(date);

    return {
        $gte: new Date(date.yyyy + '-' + date.mm + '-' + date.dd + 'T00:00:00.000Z'),
        $lte: new Date(date.yyyy + '-' + date.mm + '-' + date.dd + 'T23:59:59.000Z')
    };
}

// Get time elapsed 
function getTimeElapsed(ptime) {
    var secsOf = {'min': 60, 'hour': 60*60, 'day': 60*60*24};
    var today = new Date();
    var pday = new Date(ptime);
    var timeDiff = Math.round((today.getTime() - pday.getTime())/1000, 0);

    if (timeDiff < 0) {
        return dateTimeToString(pday);
    } else {
        if (timeDiff < 60) return "Vừa mới xong"; 
        var d = Math.round(timeDiff/secsOf.day,0);
        
        if (d >= 1) {
            if (d == 1) { return "Ngày hôm qua"; } else { return dateTimeToString(pday); }
        } else {
            if (d < 0) {
                return dateTimeToString(pday);
            } else {
                var h = Math.round(timeDiff/secsOf.hour,0);
                if (h >= 1) { 
                    return h + " giờ trước"; 
                } else {
                    var m = Math.round(timeDiff/secsOf.min,0);
                    if (m >= 1) {
                        return m + " phút trước";
                    } else {
                        return timeDiff + " giây trước";
                    }
                }
            }
        }
    }
}

// Get remain day
function getRemainDay(timestamp) {
    var today = new Date();
    var myDay = new Date(timestamp);
    var timeDiff = Math.abs(myDay.getTime() - today.getTime());
    return Math.round(timeDiff / (1000 * 3600 * 24)); 
}

//#region Exports 

exports.toLocateDateTime = toLocateDateTime;
exports.dateToString = dateToString;
exports.dateTimeToString = dateTimeToString;
exports.getISODate = getISODate;
exports.getTimeElapsed = getTimeElapsed;
exports.getRemainDay = getRemainDay;

//#endregion

//#endregion====================================


//#region============String functions============

// Get ext of file name
function getFileExtension(filename) {
    return ('.' + filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2)).toLowerCase();
}

// Get file name
function getFilename(filename) {
    return filename.replace(getFileExtension(filename), '');
}

//#region Exports

exports.getFileExtension = getFileExtension;
exports.getFilename = getFilename;

//#endregion

//#endregion====================================